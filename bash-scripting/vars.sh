#bash is case sensitive

dir = "/home-3/nwanshu1@jhu.edu/scratch/marccTR/tutorials/bash-scripting"

#single quote is literal
# when you define a variable,there cannot be space between variable and =

c='hello'

echo $c

#double quotes substitude variables

d="Big $c"
echo $d

# save contents of a command into a variable

e=$(ls $dir | wc -l)   # wc is to count sth you give to it

echo $e
